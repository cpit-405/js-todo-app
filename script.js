/*
* Khalid Alharbi, CPIT-405
* ToDo app example
*/

  // Get the DOM Elements we need to work on
  var myUlElement = document.getElementById("my-list")
  var listItems = myUlElement.children
  var addBtn = document.getElementById("add-btn")
  var deleteAllBtn = document.getElementById("delete-all-btn")
  
  // Add a close/delete button to each list item
  var i
  for(i=0; i<listItems.length; i++) {
    addCloseBtn(listItems[i])
  }
  
  // Add a new todo item
  addBtn.onclick = function() {
      addItem()
  }
  
  // Delete all todo items
  deleteAllBtn.onclick = function() {
    var myUlElement = document.getElementById("my-list")
    myUlElement.innerHTML = ''
  }
  
// Given a list item (li), add a close button (span) to delete it
function addCloseBtn(liElement) {
    var span = document.createElement("span")
    var txt = document.createTextNode("\u00D7")
    span.appendChild(txt)
    span.className = "close"
    // listen for click events on the span element to delete the parent li element
    span.onclick = function() {
      // Get the parent of the span element (ul > li > span)
      var liItem = this.parentElement // the parent of the span is li
      // delete the li element by asking its parent to remove it
      liItem.parentElement.removeChild(liItem) // the parent of li is ul
    }
    // Add the span to the li element
    liElement.appendChild(span)
  }
  
  // Add a new todo item to the list
  function addItem() {
    var inputText = document.getElementById("new-item")
    var itemName = inputText.value
    if (itemName == undefined || itemName === '') {
      return
    }
    var myUlElement = document.getElementById("my-list")
    var newElement = document.createElement("li")
    var textNode = document.createTextNode(itemName)
    newElement.appendChild(textNode)
    myUlElement.appendChild(newElement)
    addCloseBtn(newElement)
    inputText.value = ''
  }
